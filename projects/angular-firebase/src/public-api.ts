/*
 * Public API Surface of angular-firebase
 */

export * from './lib/angular-firebase.service';
export * from './lib/angular-firebase.component';
export * from './lib/angular-firebase.module';
export * from './lib/interfaces/errors';
export * from './lib/interfaces/local.firestore.interfaces';
export * from './lib/data-sources/firestore.datasource';
export * from './lib/data-sources/queries';
export * from './lib/classes/local.firestore.datasource';
