import { NgModule } from '@angular/core';
import { AngularFirebaseComponent } from './angular-firebase.component';

@NgModule({
  declarations: [AngularFirebaseComponent],
  imports: [
  ],
  exports: [AngularFirebaseComponent]
})
export class AngularFirebaseModule { }
