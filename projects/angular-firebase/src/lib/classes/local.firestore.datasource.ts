import { ILocalFirestoreItem } from '../interfaces/local.firestore.interfaces';
import { Observable } from 'rxjs';


export class LocalFirestoreDataSource {
    private root: string;
    /** Contiene todos los árboles de datos guardados en un datasource */
    private data: Array<ILocalFirestoreItem> = [];

    /** 
     * @param {string} root La raíz de los datos en nuestra firestore database.
     */
    public constructor(root: string) {
        this.root = root;
    }

    /** Devuelve el nombre del root usado actualmente */
    public getDataRootName(): string {
        return this.root;
    }
    /**
     * Añade un nuevo DataSource al objeto general
     * @param name Nombre de la subrama de datos de la que obtenemos la información
     * @param globalReference Contiene el arbol de datos en forma de objecto
     */
    public addDataSourceItem(dataSourceItem: Partial<ILocalFirestoreItem>): void {
        const newDataSource: ILocalFirestoreItem = {
            name: dataSourceItem.name,
            itemsReference: dataSourceItem.itemsReference,
            globalReference: dataSourceItem.globalReference
        };
        this.data.push(newDataSource);
    }

    /**
     * Recupera los items de una entrada en forma de observable a sus cambios.
     * Útil para construir listas refrescables
     * @param sourceItemName La busqueda se realiza por este valor con el campo "name" del item
     */
    public getItemsObservableFrom(sourceItemName: string): Observable<any>|null {
        const dataFounded = this.data.find(
            (dataSourceItem: ILocalFirestoreItem) => sourceItemName === dataSourceItem.name
        );
        if (!dataFounded) return null;
        return dataFounded.itemsReference;
    }
    /**
     * Update an entire item value inside a local datasource
     * @param dataSourceName Nombre del datasource, ej: users
     * @param itemId Id del documento para hacer update
     * @param item Nuevos parámetros con los que hacer update
     */
    public async updateItem(dataSourceName: string, itemId: string, item: ILocalFirestoreItem): Promise<void> {
        const dataSourceItemFounded = this.data.find(
            (dataSourceItem: ILocalFirestoreItem) =>
                dataSourceName === dataSourceItem.name
        );
        if (!dataSourceItemFounded) { return null; }
        try {
            await dataSourceItemFounded.globalReference.doc('/' + itemId ).update(item);
        } catch (error) {
            console.log('Update item error: ', error);
        }
    }
}