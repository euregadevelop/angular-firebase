import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IDataRoot, ILocalFirestoreItem } from '../interfaces/local.firestore.interfaces';
import { LocalFirestoreDataSource } from '../classes/local.firestore.datasource';
import { FirestoreError, LocalFirestoreError, QueryNotSupportedError } from '../interfaces/errors';
import { AddItem, AddItemAnidated, FirestoreQuery, GetItemsObservable, PutItem, SetLocalDataSources } from './queries';
import { Injectable } from '@angular/core';

/** Un dataSource es la raíz para un conjunto de datos. Para cada subrama se crea un dataSourceItem propio */
@Injectable({
  providedIn: 'root'
})
export class FirestoreDataSource {
    
    /** Raizes de datos "Firestore" de toda nuestra aplicación  */
    private dataSources: Array<LocalFirestoreDataSource> = [];

    constructor(
        private firestore: AngularFirestore
    ) { }
    
    /**
     * Única función de entrada para el dataSource
     * @param {FirestoreQuery} query - Llamada predefinida con la que el usuario realiza alguna acción
     * @throws {QueryNotSupportedError} - La query no se ha reconocido
     */
    public execute(query: FirestoreQuery): Promise<any> {
        if(query instanceof SetLocalDataSources) {
            return this.setDataSources(query.dataSources);
        } else if (query instanceof GetItemsObservable) {
            return this.getItemsObservable(query.rootName, query.itemName);
        } else if (query instanceof AddItem) {
            return this.addItem(query.item, query.collectionName);
        } else if (query instanceof AddItemAnidated) {
            return this.addItem(query.item, query.collectionRootName, query.collectionName, query.rootId);
        } else if (query instanceof PutItem) {
            return this.updateItem(query.rootName, query.itemId, query.item);
        } else {
            throw new QueryNotSupportedError('FirestoreDataSource / Query not recognized');
        }
    }

    /**
     * @description Establece los parámetros para crear los DataSources con los que funcionará toda la aplicación
     * @param {IDataRoot[]} items - Array con el nombre de las subramas de datos que usaremos en toda la aplicación
     * - Ejemplo de IDataRoot: {root: 'firestore_users', sources: ['users', 'admins']}
     * @throws {FirestoreError} - Si no se han recibido items para añadir
     * @throws {LocalFirestoreError} - Si ha habido algún problema con los datos
     */
    protected async setDataSources(items: Array<IDataRoot>): Promise<void> {

        if (!items || items.length === 0) {
            throw new FirestoreError('setDataSources / Empty items array received.');
        }

        items.forEach((item: IDataRoot) => {
            // @MF TODO comprobar que root aún no exista
            if(!item.root || item.root === '') throw new LocalFirestoreError('Error setting dataSources: item.root is empty');
            if(!item.sources || item.sources.length === 0) throw new LocalFirestoreError('Error setting dataSources: item.sources is empty');

            // Creamos la nueva dataSource local a partir de la raíz
            const newLocalDataSource = new LocalFirestoreDataSource(item.root);

            item.sources.forEach((source: string) => {
                // Referencia del firebase datasource
                const globalReference = this.firestore.collection(source);
                // Subscripción a los cambios en el datasource
                const itemsReference = this.firestore.collection(source).snapshotChanges();

                if(!globalReference || !itemsReference) throw new FirestoreError(`Error setting dataSources: firestore cant find: ${source}.`)
                
                const dataSourceItem: ILocalFirestoreItem = {
                    name: source,
                    globalReference,
                    itemsReference,
                };
                newLocalDataSource.addDataSourceItem(dataSourceItem);
            });
            this.dataSources.push(newLocalDataSource);
        });
    }

    /**
     * @description
     * @param {string} dataRootName Nombre de la raíz de datos local
     * @param {string} sourceItemName Nombre del item a recuperar
     * @throws {LocalFirestoreError} - Cuando no se encuentra el item dentro del localDataSource
     */
    protected async getItemsObservable(dataRootName: string, sourceItemName: string): Promise<Observable<any>> 
    {
        const dataSourceObject: LocalFirestoreDataSource = this.getDataSourceByRoot(dataRootName);
        const DataItemsAsObservable = dataSourceObject.getItemsObservableFrom(sourceItemName)
            .pipe(
                map(response => {
                    return Array.isArray(response) && response.length > 0 
                        ? response.map(item => item.payload.doc.data())
                        : response;
                })
            );
        if (!DataItemsAsObservable) throw new LocalFirestoreError(`Data source (${dataRootName}) item with name ${sourceItemName} not found`);
        
        return DataItemsAsObservable;
    }

    /**
     * @description Guarda en la base de datos remota de la aplicación un nuevo item
     * @param item Objecto a guardar en la base de datos
     * @param {string} rootCollectionName
     * @param {string} collectionName optional
     * @param {string} rootId optional
     * @throws {FirestoreError} - Firestore ha fallado al hacer create
     */
    protected async addItem(item: any, rootCollectionName: string, collectionName?: string, rootId?: string): Promise<void> 
    {
        const id = this.firestore.createId();
        try {
            if(collectionName) {
                if(!rootId) {
                    throw new Error('addItem / rootId not received.');
                }
                const newItem = { ...item};
                await this.firestore.collection(rootCollectionName)
                .doc(rootId)
                .collection(collectionName)
                .doc(id)
                .set(newItem);
            } else {
                const newItem = { ...item, id: id };
                await this.firestore.collection(rootCollectionName).doc(id).set(newItem);
            }
        } catch (error) {
            throw new FirestoreError('Error al añadir un item en firestore.');
        }
    }

    /**
     * @description Update de un item en la base de datos principal de la aplicación
     * @param {string} dataSourceName
     * @param {string} itemId
     * @paran item
     * @throws {LocalFirestoreError} - El método updateItem() del localDataSource ha fallado
     */
    protected async updateItem(dataSourceName: string, itemId: string, item: any): Promise<void> 
    {
        try {
            const dataSource = this.getDataSourceByRoot(dataSourceName);
            await dataSource.updateItem(dataSourceName, itemId, item);
        } catch (error) {
            throw new LocalFirestoreError('Error al modificar un item en firestore.');
        }
    }

    /**
     * @description Recupera una dataSOurce a partir del nombre de su root
     * @param {string} dataRootName Nombre de la raíz, corresponde con el primer nivel de la RealtimeDatabase
     * @throws {LocalFirestoreError} - Si no encuentra el localDataSource con el root indicado
     */
    private getDataSourceByRoot(dataRootName: string): LocalFirestoreDataSource {
        const dataSourceObject = this.dataSources.find(
            (dataSource: LocalFirestoreDataSource) => {
                return dataSource.getDataRootName() === dataRootName;
            }
        );

        if (!dataSourceObject) throw new LocalFirestoreError(`getDataSourceByRoot / Data source with root ${dataRootName} not found.`);

        return dataSourceObject;
    }
}