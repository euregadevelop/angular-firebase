import { IDataRoot } from '../interfaces/local.firestore.interfaces';

export class FirestoreQuery {}
export class SetLocalDataSources extends FirestoreQuery {
    constructor(readonly dataSources: Array<IDataRoot>) { super() }
};
export class GetItemsObservable extends FirestoreQuery {
    constructor (readonly rootName: string, readonly itemName: string) { super() }
};

export class AddItem extends FirestoreQuery { 
    constructor(readonly collectionName: string, readonly item: any) { super() }
};

export class AddItemAnidated extends FirestoreQuery {
    constructor (
        readonly collectionRootName: string, 
        readonly collectionName: string, 
        readonly item: any,
        readonly rootId: string
    ) { super() }
};

export class PutItem extends FirestoreQuery {
    constructor(readonly rootName: string, readonly itemId: string, readonly item: any) { super() }
};