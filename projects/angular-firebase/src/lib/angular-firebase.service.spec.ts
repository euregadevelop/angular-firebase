import { TestBed } from '@angular/core/testing';

import { AngularFirebaseService } from './angular-firebase.service';

describe('AngularFirebaseService', () => {
  let service: AngularFirebaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AngularFirebaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
