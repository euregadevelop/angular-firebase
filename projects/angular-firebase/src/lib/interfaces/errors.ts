export class FireAuthError extends Error {
    constructor(message?: string) {
        super();
    }
}

export class LoginError extends Error {
    constructor(message?: string) {
        super();
    }
}

export class FirestoreError extends Error {
    constructor(message?: string) {
        super();
    }
}

export class LocalFirestoreError extends Error {
    constructor(message?: string) {
        super(message);
        this.name = "Local Firestore Error";
    }
}

export class QueryNotSupportedError extends Error {
    constructor(message?: string) {
        super();
    }
}