import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

export interface ILocalFirestoreItem {
    // Nombre de la subrama de datos de la que obtenemos la información
    name: string;
    // Observable a los cambios en los datos recuperados
    itemsReference: Observable<any>;
    // Contiene el arbol de datos en forma de objecto
    globalReference: AngularFirestoreCollection<any>;
}

/** Un objeto datasource se encuentra debajo de una root de datos */
export interface IDataRoot {
    // Nombre de la subrama principal de la que nacen los datasources 
    root: string;
    // Lista de todos los sources que queremos usar en nuestra aplicación dentro de una raíz 
    sources: Array<string>;
}

export interface FirestoreEntity<T> {
    id: string;
    data: T;
}